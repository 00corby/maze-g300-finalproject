using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int gunDamage = 1;


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            //or gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
            //or gameObject.SetActive(false);
        }

    }

}
