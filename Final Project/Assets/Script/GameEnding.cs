using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float timeValue = 90;
    public Text timerText;

    public Text healthText;

    public int currentHealth = 3;

    public int damageAmount = 3;

    public string gameLevelLose;
    




    // Code for Timer https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/ 

    // Update is called once per frame
    void Update()
    {
        if (timeValue > 0)
        {
            timeValue -= Time.deltaTime;
        }
        else
        {
            timeValue = 0;
            SceneManager.LoadScene(gameLevelLose);
        }

        DisplayTime(timeValue);
        healthText.text = "Health:" + currentHealth;
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            currentHealth -= damageAmount;

            //Check if health has fallen below zero
            if (currentHealth <= 0)
            {
                //if health has fallen below zero, deactivate it 
                SceneManager.LoadScene(gameLevelLose);
            }
        }
    }


    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerText.text = "Time Left: " + string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
