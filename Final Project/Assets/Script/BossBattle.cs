using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class BossBattle : MonoBehaviour
{
	//The box's current health point total
	public int currentHealth = 3;

	public Transform player;

	public LayerMask whatIsPlayer;

	public string gameLevelWin;

	public float timeBetweenAttacks;
	bool alreadyAttacked;
	public GameObject projectile;

	public float sightRange, attackRange;
	public bool playerInSightRange, playerInAttackRange;

	private void Awake()
	{
		player = GameObject.Find("FPSController").transform;
	}

	private void Update()
	{
		//Check for sight and attack range
		playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
		playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

		if (playerInAttackRange && playerInSightRange) AttackPlayer();
	}


	private void AttackPlayer()
	{

		transform.LookAt(player);

		if (!alreadyAttacked)
		{
			///Attack code here
			Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
			rb.AddForce(transform.forward * 16f, ForceMode.Impulse);
			rb.AddForce(transform.up * 3f, ForceMode.Impulse);
			///End of attack code

			alreadyAttacked = true;
			Invoke(nameof(ResetAttack), timeBetweenAttacks);
		}
	}
	private void ResetAttack()
	{
		alreadyAttacked = false;
	}

	public void Damage(int damageAmount)
	{
		//subtract damage amount when Damage function is called
		currentHealth -= damageAmount;

		//Check if health has fallen below zero
		if (currentHealth <= 0)
		{
			//if health has fallen below zero, deactivate it 
			SceneManager.LoadScene(gameLevelWin);
		}
	}
}